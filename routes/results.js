var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    var db = req.db;
    var user = req.query.user;
	db.smembers(user, function(err, reply) {
		res.json({streams: reply});
	});
});

module.exports = router;
